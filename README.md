# Danalock Patio Slider Adaptor

Adaptor plate to fit Danalock to Patio Slider

## Introduction

![Before and After](img/0-Intro.jpg)

This is a guide for printing your own smart lock plate for the [Danalock V3](https://danalock.com/products/danalock-v3-smart-lock) to fit onto a [Lockwood Onyx Sliding door lock](https://www.lockweb.com.au/au/en/products/sliding-door-locks-and-hardware/onyx-sliding-door-lock) with a resin printer.

Designed to replace the "Slim Inner Pull" handle of the Onyx door lock. One could use it to replace the bigger "handle" version too, if there is another handle or grab point on the door. Else the design would need to be modified to introduce a handle.

My motivation behind this project came as there was no off the shelf smart lock that would fit this often used sliding door. After inspecting the locking mechanism, I realised it would be trivial to manufacture an adaptor to fit a generic smart deadbolt. Chose the Danalock V3 Z-wave, for it's Z-wave compatibility and looked like it would be easy to retro fit in this case. Easily set it up with [Zwavejs2mqtt](https://github.com/zwave-js/zwavejs2mqtt) and [Home Assistant](https://home-assistant.io) to use with HomeKit.

I bought my Danalock V3 from [Oz Smart Things](https://www.ozsmartthings.com.au), few versions available: [Z-wave (AU) & Bluetooth](https://www.ozsmartthings.com.au/products/salto-danalock-v3-deadbolt), [Zigbee & Bluetooth](https://www.ozsmartthings.com.au/products/danalock-v3-deadbolt-zigbee), [Bluetooth](https://www.ozsmartthings.com.au/products/danalock-v3-deadbolt-bluetooth) and [HomeKit](https://www.ozsmartthings.com.au/products/danalock-v3-deadbolt-homekit).

The design is for a left opening door. It can be mounted on the right, but the battery cover will be at the top. Some minor modifications can be made to the original Fusion 360 design (rotate sketches of Danalock base notches and screw holes, RHS STL included). The handle plate body or Danalock base body can be used as a starting point for other smart deadbolt and door handle combinations.

The designs may work on FDM printers too, but not tested. 

To modify the designs, use the free [Autodesk Fusion 360 Personal](https://www.autodesk.com.au/products/fusion-360/personal)

### Requirements

* Danalock V3 + Lockwood Onyx Sliding door lock (slim)
* Knowledge on using Autodesk Fusion 360 if making modifications
* Resin 3D printer (FDM may work), and the knowledge on how to use it.
* Resin. [Siraya Tech Build](https://siraya.tech/collections/australia-au/products/build-1kg-smoky-black-au-high-resolution-engineering-3d-printing-resin) works really well for the plate. The pin requires a much stronger resin such as [Siraya Tech Blu](https://siraya.tech/collections/australia-au/products/blu-by-siraya-tech-for-lcd-resin-printers-au-1kg), else it will snap.
* A flat small fine file; to file down supports and imperfections
* 1200 grit water resistant sand paper; to sand down imperfections
* Screwdriver set with various Philips-head sizes

## Steps to Make

### 1. Dismantle and inspect the door lock handle

![Doorlock](img/1-DoorLock.jpg) 

Remove the door lock handle. Inspect how it works. If a screw driver can be inserted into the barrel and turned by hand, a smart lock will work.

### 2. Inspect the smart lock

![Smart lock](img/2.0-FlangeView.jpg)

This Danalock has large flange at the base, which won't play nicely with the sliding door.

![Smart lock](img/2.1-RemoveFlange.jpg)

Luckily, the base can be easily removed, by removing the 3 screws shown in the diagram. In this case the best course of action is to replicate the mounting plate pattern and integrate into the adaptor plate we're designing. Using the Baldwin/Kwikset tailpiece adaptor (black) included with the lock will make designing the pin a lot simpler.

### 3. Prototyping (A step if making a custom lock plate)

![Prototyping](img/3-Prototyping.jpg)

Bring out the callipers and ruler, measure everything. Check for clearances around the door, lock etc. Make a print and modify with a file/drill/knife until it fits. Apply those adjustments to the model and print again. Keep iterating until successful (took me 15 iterations). To speed up the prototyping process on a resin printer; one can print the lock plate base and handle plate separately flat on bed.

### 4. Print the Locking Pin

![Pin](img/4-LockingPin.jpg)

The pin must be printed in a strong material (e.g Siraya Tech Blu), else it will snap if the lock attempts to lock while the door is still open. Once the pin is printed, some filing and sanding may be required to fit properly. Test that it fits into the door lock and the tailpiece adaptor of the Danalock.
* Pole with Supports for Resin Printing STL [LockPole+ResinSupports.stl](LockPole+ResinSupports.stl)
* Pole STL [LockPole.stl](LockPole.stl)
* Pole Autodesk Fusion 360 file [LockPole.f3d](LockPole.f3d)

### 5. Print the Plate

![Plate](img/5-Plate.jpg)

It's best to print the plate with a material with more accuracy (e.g Siraya Tech Build). If modifying the model, make sure to add the print supports to the back side of the plate for the cleanest finish and best accuracy to mate the Danalock base plate with the Danalock. The model comes designed with threaded screw holes that will fit the M4 screws included with the Onyx handle. If printing with an FDM printer, these holes may require tapping.
* Plate with Supports for Resin Printing STL [LockPlate+ResinSupports.stl](LockPlate+ResinSupports.stl)
* Plate STL [LockPlate.stl](LockPlate.stl) or [LockPlate-RHS.stl](LockPlate-RHS.stl) (Right Hand Side)
* Plate Autodesk Fusion 360 file [LockPlate.f3d](LockPlate.f3d)

### 6. Post Processing

![Post Processing](img/6-PostProcessing.jpg)

1. Once printed, remove the supports and cure the print.
2. File down any leftover supports or lumps.
3. Sanding down any imperfections using P1200 water resistant sandpaper, while wet, till all the visible surfaces are smooth. 4. Check everything fits and screws together.

### 7. Painting

![Painting](img/7-Painting.jpg)

Use a spray can that matches the rest of the door handle for an integrated look. In this case `Dulux Dura Max` in `semi gloss` `satin black` worked well, when sprayed on dry from a distance. Practice on other objects first, but if a mistake is made, the P1200 sand paper can be used to sand back the paint again. Spray from all angles, the back isn't as important.

### 8. Assembly

![Assembly](img/8-Assembly.jpg)

1. Screw the Danalock to the plate, making sure to thread through the battery removal ribbon. 
2. Screw the plate to the handle assembly.

### 9. Finish

![Final](img/9-Final.jpg)

1. Insert the locking pin, make sure the door is in the unlocked position. 
2. Twist the Danalock so that the tailpiece adaptor at the back aligns with the locking pin and carefully press it on.  
3. Calibrate the lock using the app for the closed and open positions. 
5. Test it works.
4. Test the scenario where the lock attempts to lock while the door is still open. If the locking pin doesn't snap, then it's a success.

Demonstration Video:

[![Watch the video](img/Doorlock.png)](img/Doorlock.mov)